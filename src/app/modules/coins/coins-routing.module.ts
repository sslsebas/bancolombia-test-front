import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AvailableCoinsComponent } from './available-coins/available-coins.component';
import { ListCoinsComponent } from './list-coins/list-coins.component';

const routes: Routes = [
  {
    path: 'list',
    component: ListCoinsComponent,
  },
  {
    path: 'list-avaibles',
    component: AvailableCoinsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CoinsRoutingModule {}
