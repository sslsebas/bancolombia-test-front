import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
// import { TranslateService } from '@ngx-translate/core';
import { Coin } from 'app/interfaces/coin.interface';
import { CoinsService } from 'app/services/apiBanco/coins.service';
import { DeleteCoinComponent } from '../delete-coin/delete-coin.component';
import { SetCoinComponent } from '../set-coin/set-coin.component';

@Component({
  selector: 'app-list-coins',
  templateUrl: './list-coins.component.html',
  styleUrls: ['./list-coins.component.css'],
})
export class ListCoinsComponent implements OnInit {
  get coins(): Coin[] {
    return this.coinService.coins;
  }

  constructor(
    public dialog: MatDialog,
    private coinService: CoinsService // public translate: TranslateService
  ) {
    this.coinService.getCoins();
  }

  ngOnInit(): void {}

  addCoin() {
    const dialogRef = this.dialog.open(SetCoinComponent, {
      panelClass: 'modal',
    });
  }

  updateCoin(coin: Coin) {
    const dialogRef = this.dialog.open(SetCoinComponent, {
      panelClass: 'modal',
      data: {
        coin: coin,
      },
    });
  }

  deleteCoin(coinId: string) {
    const dialogRef = this.dialog.open(DeleteCoinComponent, {
      panelClass: 'modal',
    });
    dialogRef.afterClosed().subscribe((resp) => {
      resp.isDeleted && this.coinService.deleteCoin(coinId);
    });
  }
}
