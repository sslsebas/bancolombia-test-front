import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetCoinComponent } from './set-coin.component';

describe('SetCoinComponent', () => {
  let component: SetCoinComponent;
  let fixture: ComponentFixture<SetCoinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetCoinComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetCoinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
