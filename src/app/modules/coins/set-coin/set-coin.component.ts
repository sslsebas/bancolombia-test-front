import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Coin } from 'app/interfaces/coin.interface';
import { CoinsService } from 'app/services/apiBanco/coins.service';

@Component({
  selector: 'app-set-coin',
  templateUrl: './set-coin.component.html',
  styleUrls: ['./set-coin.component.css'],
})
export class SetCoinComponent implements OnInit {
  formCoin: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<SetCoinComponent>,
    private coinService: CoinsService,
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.formCoin = formBuilder.group({
      name: ['', Validators.required],
      symbol: ['', Validators.required],
      value: ['', Validators.required],
    });
  }

  validateForm(): boolean {
    return this.formCoin.valid;
  }

  createCoin() {
    if (this.validateForm()) {
      const newCoin: Coin = this.formCoin.value;
      this.coinService.createCoin(newCoin);
      this.formCoin.reset();
      this.closeDialog();
    }
  }

  updateCoin() {
    if (this.validateForm()) {
      const updatedCoin: Coin = this.formCoin.value;
      this.coinService.updateCoin(this.data.coin._id, updatedCoin);
      this.formCoin.reset();
      this.closeDialog();
    }
  }

  setFormValues() {
    this.formCoin.patchValue(this.data.coin);
  }

  ngOnInit(): void {
    this.data && this.setFormValues();
  }

  closeDialog() {
    this.dialogRef.close();
  }
}
