import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoinsRoutingModule } from './coins-routing.module';
import { AvailableCoinsComponent } from './available-coins/available-coins.component';
import { ListCoinsComponent } from './list-coins/list-coins.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { DeleteCoinComponent } from './delete-coin/delete-coin.component';
import { SetCoinComponent } from './set-coin/set-coin.component';
// import { TranslateHttpLoader } from '@ngx-translate/http-loader';
// import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
// import { HttpLoaderFactory } from 'app/app.module';

@NgModule({
  declarations: [
    AvailableCoinsComponent,
    ListCoinsComponent,
    DeleteCoinComponent,
    SetCoinComponent,
  ],
  imports: [
    CommonModule,
    CoinsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    // TranslateHttpLoader,
    // TranslateModule.forRoot({
    //   defaultLanguage: 'en',
    //   loader: {
    //     provide: TranslateLoader,
    //     useFactory: HttpLoaderFactory,
    //     deps: [HttpClient],
    //   },
    // }),
  ],
})
export class CoinsModule {}
