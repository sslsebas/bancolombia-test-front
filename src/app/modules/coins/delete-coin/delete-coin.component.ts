import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-delete-coin',
  templateUrl: './delete-coin.component.html',
  styleUrls: ['./delete-coin.component.css'],
})
export class DeleteCoinComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<DeleteCoinComponent>) {}

  ngOnInit(): void {}

  closeDialog(isDelete: boolean) {
    this.dialogRef.close({ isDeleted: isDelete });
  }
}
