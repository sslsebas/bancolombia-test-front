import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Country } from 'app/interfaces/country.interface';
import { CountriesService } from 'app/services/apiBanco/countries.service';
import { AddCountryComponent } from '../add-country/add-country.component';
import { ListItemsComponent } from '../list-items/list-items.component';

@Component({
  selector: 'app-list-countries',
  templateUrl: './list-countries.component.html',
  styleUrls: ['./list-countries.component.css'],
})
export class ListCountriesComponent implements OnInit {
  get countries(): Country[] {
    return this.countryService.countries;
  }
  constructor(
    public dialog: MatDialog,
    private countryService: CountriesService
  ) {
    this.countryService.getCountries();
  }

  ngOnInit(): void {}

  openDialog() {
    const dialogRef = this.dialog.open(AddCountryComponent, {
      panelClass: 'modal',
    });
  }
}
