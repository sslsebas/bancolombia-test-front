import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Coin } from 'app/interfaces/coin.interface';
import { Country } from 'app/interfaces/country.interface';
import { CoinsService } from 'app/services/apiBanco/coins.service';
import { CountriesService } from 'app/services/apiBanco/countries.service';

@Component({
  selector: 'app-add-country',
  templateUrl: './add-country.component.html',
  styleUrls: ['./add-country.component.css'],
})
export class AddCountryComponent implements OnInit {
  formCountry: FormGroup;

  get coins(): Coin[] {
    return this.coinService.coins;
  }

  constructor(
    public dialogRef: MatDialogRef<AddCountryComponent>,
    private coinService: CoinsService,
    private countryService: CountriesService,
    private formBuilder: FormBuilder
  ) {
    this.formCountry = formBuilder.group({
      name: ['', Validators.required],
      coins: ['', Validators.required],
      users: ['', Validators.required],
    });
    this.coinService.getCoins();
  }

  validateForm(): boolean {
    return this.formCountry.valid;
  }

  createCountry() {
    if (this.validateForm()) {
      const newCountry: Country = this.formCountry.value;
      console.log(this.formCountry.value);

      this.countryService.createCountry(newCountry);
      this.formCountry.reset();
      this.closeDialog();
    }
  }

  ngOnInit(): void {}

  closeDialog() {
    this.dialogRef.close();
  }
}
