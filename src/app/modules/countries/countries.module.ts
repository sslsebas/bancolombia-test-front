import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CountriesRoutingModule } from './countries-routing.module';
import { AddCountryComponent } from './add-country/add-country.component';
import { ListCountriesComponent } from './list-countries/list-countries.component';
import { ListItemsComponent } from './list-items/list-items.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';

@NgModule({
  declarations: [
    AddCountryComponent,
    ListCountriesComponent,
    ListItemsComponent,
  ],
  imports: [
    CommonModule,
    CountriesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
  ],
})
export class CountriesModule {}
