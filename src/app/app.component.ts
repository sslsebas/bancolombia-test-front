import { Component } from '@angular/core';
// import { TranslateService } from '@ngx-translate/core';
// import { TranslateService } from '../../node_modules/@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'bancolombia-test-front';

  // constructor(public translate: TranslateService) {
  //   // this.translate.addLangs(['es', 'en']);
  //   // this.translate.setDefaultLang('en');
  //   this.translate.use('en');
  // }
}
