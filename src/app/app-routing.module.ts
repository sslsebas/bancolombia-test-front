import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BackComponent } from './pages/back/back.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: 'coins',
    component: BackComponent,
    loadChildren: () =>
      import('./modules/coins/coins.module').then((m) => m.CoinsModule),
  },
  {
    path: 'countries',
    component: BackComponent,
    loadChildren: () =>
      import('./modules/countries/countries.module').then(
        (m) => m.CountriesModule
      ),
  },
  {
    path: 'users',
    component: BackComponent,
    loadChildren: () =>
      import('./modules//users/users.module').then((m) => m.UsersModule),
  },
  {
    path: '**',
    redirectTo: 'login',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
