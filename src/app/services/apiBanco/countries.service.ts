import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Country } from 'app/interfaces/country.interface';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root',
})
export class CountriesService {
  private _serviceURL: string = environment.apiURL;
  private _countries: Country[] = [];

  get countries(): Country[] {
    return [...this._countries];
  }

  constructor(private http: HttpClient) {}

  /**
   * @description Get all coins
   */
  getCountries() {
    this.http
      .get<Country[]>(`${this._serviceURL}/countries`)
      .subscribe((resp) => {
        this._countries = resp;
      });
  }

  createCountry(newCountry: Country) {
    this.http
      .post(`${this._serviceURL}/countries`, newCountry)
      .subscribe((createdCountry: any) => {
        this._countries.push(createdCountry);
      });
  }
}
