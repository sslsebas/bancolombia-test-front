import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Coin } from 'app/interfaces/coin.interface';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root',
})
export class CoinsService {
  private _serviceURL: string = environment.apiURL;
  private _coins: Coin[] = [];

  get coins(): Coin[] {
    return [...this._coins];
  }

  constructor(private http: HttpClient) {}

  /**
   * @description Get all coins
   */
  getCoins() {
    this.http.get<Coin[]>(`${this._serviceURL}/coins`).subscribe((resp) => {
      this._coins = resp;
    });
  }

  createCoin(newCoin: Coin) {
    this.http
      .post(`${this._serviceURL}/coins`, newCoin)
      .subscribe((createdCoin: any) => {
        this._coins.push(createdCoin);
      });
  }

  deleteCoin(_id: string) {
    this.http
      .post(`${this._serviceURL}/coins/delete`, { _id })
      .subscribe((deletedCoin: any) => {
        this._coins = this._coins.filter((coin) => coin._id !== _id);
      });
  }

  updateCoin(_id: string, fields: {}) {
    this.http
      .post(`${this._serviceURL}/coins/update`, { _id, fields })
      .subscribe((updateCoin: any) => {
        // const pos = this._coins.findIndex((coin) => coin._id === _id);
        this.getCoins(); //TODO: fix this
      });
  }
}
