import { Country } from './country.interface';

export interface User {
  _id?: number;
  name: string;
  country: Country;
}
