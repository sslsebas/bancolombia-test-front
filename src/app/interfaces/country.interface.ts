import { Coin } from './coin.interface';
import { User } from './user.interface';

export interface Country {
  _id?: number;
  name: string;
  coins?: Coin[];
  users?: User[];
}
