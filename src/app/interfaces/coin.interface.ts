export interface Coin {
  _id: string;
  name: string;
  symbol: string;
  value: number;
}
